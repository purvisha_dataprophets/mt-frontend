import React, { useState } from 'react';
import { Button, Container, Form } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

import './register.css';

const Register = ({ updateUser }) => {
    console.log("into regisyer")
    const history = useHistory();

    const [user, setUser] = useState({
        name: '',
        email: '',
        password: '',
        reEnterPassword: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUser({
            ...user,
            [name]: value
        });
    }

    const register = async (e) => {
        e.preventDefault();
        const { name, email, password, reEnterPassword } = user;
        if( name && email && password && reEnterPassword ) {
            if (password === reEnterPassword) {
                try {
                    const registerResponse = await axios.post('http://localhost:5000/register', user);
                    if (registerResponse && registerResponse.status && registerResponse.status === 200) {
                        updateUser(registerResponse.data.user);
                        history.push('/login');
                    } else {
                        alert(registerResponse.data.message);
                    }
                } catch(err) {
                    alert(err);
                }
            } else {
                alert('password not match');
            }
        } else {
            alert('invalid input');
        }
    }

    const login = () => {
        history.push('/login');
    }

    return (
        <>
       
        <Container className='w-25 containerBlue'>
            <Form>
            <div className="header-form">
                        <h4 className="text-primary text-center"><i className="fa fa-user-circle" style={{ fontSize: "110px" }}></i></h4>
                        <div className="image">
                        </div>
            </div>
                <Form.Group className="mb-3" controlId="formBasicName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" name='name' required
                    value={user.name} onChange={ handleChange } placeholder="Enter Name" />
                    <Form.Control.Feedback type="invalid">
                    Please enter name.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" name='email' required
                    value={user.email} onChange={ handleChange } placeholder="Enter email" />
                    <Form.Control.Feedback type="invalid">
                    Please enter email.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" name='password' required
                    value={user.password} onChange={ handleChange } placeholder="Password" />
                    <Form.Control.Feedback type="invalid">
                    Please enter password.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicReEnterPassword">
                    <Form.Label>Re-enter Password</Form.Label>
                    <Form.Control type="password" name='reEnterPassword' required
                    value={user.reEnterPassword} onChange={ handleChange } placeholder="Re-enter Password" />
                    <Form.Control.Feedback type="invalid">
                    Please re-enter password.
                    </Form.Control.Feedback>
                </Form.Group>
                
                <Form.Group className="mb-3 text-center">
                <Button variant="primary" type="submit" onClick={register}>
                    Register
                </Button>
                </Form.Group>
               
                <Form.Group className="mb-3 text-center">
                    <Form.Label className='mt-3'>OR</Form.Label>
                </Form.Group>
                <Form.Group className="mb-3 text-center">
                <Button variant="primary" type="button"  onClick={login}>
                    Login
                </Button>                
                </Form.Group>
                
                
            </Form>
        </Container>







        {/* <form>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationDefault01">First name</label>
      <input type="text" class="form-control" id="validationDefault01" placeholder="First name" value="Mark" required/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefault02">Last name</label>
      <input type="text" class="form-control" id="validationDefault02" placeholder="Last name" value="Otto" required/>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefaultUsername">Username</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroupPrepend2">@</span>
        </div>
        <input type="text" class="form-control" id="validationDefaultUsername" placeholder="Username" aria-describedby="inputGroupPrepend2" required/>
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="validationDefault03">City</label>
      <input type="text" class="form-control" id="validationDefault03" placeholder="City" required/>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationDefault04">State</label>
      <input type="text" class="form-control" id="validationDefault04" placeholder="State" required/>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationDefault05">Zip</label>
      <input type="text" class="form-control" id="validationDefault05" placeholder="Zip" required/>
    </div>
  </div>
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required/>
      <label class="form-check-label" for="invalidCheck2">
        Agree to terms and conditions
      </label>
    </div>
  </div>
  <button class="btn btn-primary" type="submit">Submit form</button>
</form> */}

{/*<div className="container">
 <div className="form-box">
    <div className="header-form">
        <h4 className="text-primary text-center"><i className="fa fa-user-circle" style={{ fontSize: "110px" }}></i></h4>
        <div className="image">
        </div>
    </div>
    <div className="body-form">
        
        <form>
            <div className="input-group mb-3">
                  <input className="form-control" type="email" name='email'
                    onChange={handleChange} placeholder="Email" required/>
            </div>
            <div className="input-group mb-3">
                  <input className="form-control" type="text" name='name'
                    onChange={handleChange} placeholder="name" required/>
            </div>
            <div className="input-group mb-3">

                <input type="password" name='password' 
                    onChange={handleChange} className="form-control" placeholder="Password" required />
            </div>
            
            <div className="message">
            </div>
       
        <div className="social">
            <button type="submit" className="btn btn-secondary btn-block"

                onClick={register}
                >REGISTER</button>
        </div>
        </form>
    </div>
</div> 
</div>*/}
</>
    )
}

export default Register;