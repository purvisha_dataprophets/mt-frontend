import React, { useEffect, useState } from 'react';
import './homepage.css';
import { Button, Col, Container, Form, Row, Table, Tabs, Tab, Sonnet } from 'react-bootstrap';
import axios from 'axios';
import { format } from 'prettier';
import Multiselect from 'multiselect-react-dropdown';
import download  from 'downloadjs';
const Homepage = ({ updateUser }) => {


    const user = JSON.parse(localStorage.getItem('user'));
    var option = [{ name: 'Option 1️', id: 1 }, { name: 'Option 2️', id: 2 }];
    const [selected, setSelected] = useState([])
    const [options, setOptions] = useState([])
    const [activeTab, setActiveTab] = useState(true)
    const [tab, setTab] = useState({
        upload: false,
        show: false
    })
    const [userList, setUsers] = useState([]);
    const [fileList, setFileList] = useState([]);
    const [permittedUsers, setPermitterUsers] = useState([]);
    const [selectedFile, setSelectedFile] = useState('');

    const headers = {
        'authorization': `Bearer ${user.token}`,
    };

    const getFileList = async () => {
        const res = await axios.get('http://localhost:5000/v1/listFile', { headers });
        setFileList(res.data.files);
    }
    const getUserList = async () => {
        const res = await axios.get('http://localhost:5000/v1/listUser', { headers });

        // res.data.users.unshift({_id: null, name: 'select user'})
        setUsers(res.data.users);
        let opt = [...options];
        res.data.users.map((el, i) => {
            if (el.name != user.name) {
                opt.push({ name: el.name, id: el._id })
            }
        })
        setOptions(opt);
    }
    useEffect(() => {
        getUserList();
        getFileList();
    }, []);

    const downloadCSV = async (fileId) => {
   
     const res = await axios.get(`http://localhost:5000/v1/fileDetail?userId=${user._id}&fileId=${fileId}`, { headers });
     if (res.data.data) {
        const response = await fetch(res.data.data.filePath);
        console.log("response",response);
        const data = await response.text();
        const blob = new Blob([data], { type: "data:text/csv;charset=utf-8," });
        const blobURL = window.URL.createObjectURL(blob);
        
        // Create new tag for download file
        const anchor = document.createElement("a");
        anchor.download = "name.csv";
        anchor.href = blobURL;
        anchor.dataset.downloadurl = ["text/csv", anchor.download, anchor.href].join(
          ":"
        );
        anchor.click();
        
        // Remove URL.createObjectURL. The browser should not save the reference to the file.
        setTimeout(() => {
          // For Firefox it is necessary to delay revoking the ObjectURL
          URL.revokeObjectURL(blobURL);
        }, 100);
     }
      };
    const viewFile = async (fileId) => {
        try {
            const res = await axios.get(`http://localhost:5000/v1/fileDetail?userId=${user._id}&fileId=${fileId}`, { headers });
            if (res.data.data) {
                window.URL = window.webkitURL || window.URL;
                // const temp = res.blob();
                // console.log("temp",temp)
                var a = document.createElement("a");
                var file = new Blob([res.data.data.data]);
                a.href = window.URL.createObjectURL(file);
                // a.download = res.data.data.fileName;
                // a.click();
                download(res.data, res.data.data.fileName, res.data.data.mimetype);

            } else {
                alert('sorry!!, you do not have access')
            }
        } catch (err) {
            console.log(err);
        }
    }

    const selectUser = (e) => {
        setPermitterUsers(e.target.value)
    }


   
    const uploadFile = async (e) => {
        try {
            if (permittedUsers.length) {

                if (userList && userList.length) setPermitterUsers(userList[0]._id);
                else {
                    alert('please select user to give permission');
                    e.preventDefault()
                    return;
                }
            }
            const formData = new FormData();
            formData.append('uploadFile', selectedFile)
            formData.append('permittedUsers', permittedUsers);

            const res = await axios.post('http://localhost:5000/v1/uploadFile', formData, { headers });
            if (res.status === 200) {
                alert('file uploaded successfully!!')
                // var a = document.createElement("a");
                // var file = new Blob([res.data.data]);
                // a.href = URL.createObjectURL(file);
                // a.download = res.data.fileName;
                // a.click();
            } else {
                alert('something went wrong!!')
            }
        } catch (err) {
            console.log("error")
        }
    }
    const onSelect = (selectedList, selectedItem) => {
        let users = [...permittedUsers];
        users.push(selectedItem.id);
        setPermitterUsers(users);
    }
    const onRemove = (selectedList, removedItem) => {
        let users = [...permittedUsers];
        users.pop(removedItem._id);
        setPermitterUsers(users);
    }

    return (
        <>
            {/* <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">
  <Tab eventKey="home" title="Home">
      <p>dfdfd</p>
  </Tab>
  <Tab eventKey="profile" title="Profile">
  <p>hghghghghghgh</p>
  </Tab>
  <Tab eventKey="contact" title="Contact" disabled>
  <p>dfdfd</p>
  </Tab>
</Tabs> */}
            <div className='homepage'>
                <Container className='w-75'>
                    <Row>
                        <Col md={6} style={{ marginRight: "auto" }}>
                            <h1>Hello {user.name}!!</h1>
                        </Col>
                        <Col md={6}>
                            <Button className="pull-right" size="sm" type='button'
                                onClick={() => updateUser({})}>
                                Logout
                            </Button>
                        </Col>
                    </Row>

                    <br></br>
                    <Tabs
                        transition={true}
                        id="noanim-tab-example"
                        className="mb-3"
                        variant="pills"
                    >

                        <Tab eventKey="Upload" title="Upload">
                            <Container className='uploadFile p-4 bgcolor'>
                                <Form onSubmit={uploadFile} encType='multipart/form-data'>
                                    <Row>
                                        <Col>
                                            <h4>
                                                Upload File
                                            </h4>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group controlId="formFile" className="mb-3">
                                                <Form.Control type="file" multiple name='uploadFile' onChange={(e) => {
                                                    setSelectedFile(e.target.files[0])
                                                }} />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            {/* <Form.Select onChange={selectUser}>
                        {
                            userList && userList.length ? 
                            userList.map((value) => (
                                value.name != user.name ? 
                                <option key={value._id} value={value._id}>{value.name}</option>
                                :
                                null
                            ))
                            : 
                            <option disabled>no option availble</option>
                        }
                        </Form.Select> */}
                                            <Multiselect
                                                options={options} // Options to display in the dropdown
                                                // selectedValues={setSelected(...selected,{user:"6"})} // Preselected value to persist in dropdown
                                               onSelect={onSelect} // Function will trigger on select event
                                               onRemove={onRemove} // Function will trigger on remove event
                                               displayValue="name" // Property name to display in the dropdown options
                                            />
                                        </Col>
                                        <Col>
                                            <Button type='submit'>Upload</Button>
                                        </Col>
                                    </Row>
                                </Form>
                            </Container>
                        </Tab>
                        <Tab eventKey="Show" title="Show List" onClick={()=> getFileList}>
                            <Container className='p-4 bgcolor min-vh-100'>
                                <Table striped bordered hover size="sm" responsive>
                                    <thead>
                                        <tr>
                                            <th>File Path</th>
                                            <th>File Name</th>
                                            <th>Uploaded By</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {fileList && fileList.length ?
                                            fileList.map((item) => (
                                                <tr key={item.id}>
                                                    <td>{item.filePath}</td>
                                                    <td>{item.fileName}</td>
                                                    <td>{item.uploadedBy}</td>
                                                    <td>
                                                        <Button type='button' onClick={() => { 
                                                            viewFile(item._id)
                                                            // downloadCSV(item._id)
                                                             }}> View </Button>
                                                    </td>
                                                    <td />
                                                </tr>
                                            ))
                                            : <tr>
                                                <td colSpan={5} align='center'>
                                                    Please upload file
                                                </td>
                                            </tr>}
                                    </tbody>
                                </Table>
                            </Container>
                        </Tab>
                    </Tabs>
                </Container>
                <br></br>

                <br></br>
            </div>
        </>
    )
}

export default Homepage;