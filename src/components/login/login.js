import React, { useState } from 'react';
import './login.css';
import { Button, Container, Form } from 'react-bootstrap';
import axios from 'axios';
import { useHistory } from 'react-router-dom';

const Login = ({ updateUser }) => {
    const history = useHistory();

    const [user, setUser] = useState({
        email: '',
        password: '',
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUser({
            ...user,
            [name]: value
        });
    }

    const login = async (e) => {
        e.preventDefault();
        const { email, password } = user;
        if (email && password) {
            try {
                const loginResponse = await axios.post('http://localhost:5000/login', user);
                console.log(loginResponse)
                if (loginResponse && loginResponse.data && loginResponse.data.user) {
                    updateUser(loginResponse.data.user);
                    history.push('/');
                } else {
                    alert(loginResponse.data.message);
                }
            } catch (err) {
                alert(err);
            }
        } else {
            alert('provide login details');
        }
    }

    const register = () => {
        history.push('/register');
    }

    return (
        <>

            <Container className="w-25 containerBlue">
            <Form>
            <div className="header-form">
                        <h4 className="text-primary text-center"><i className="fa fa-user-circle" style={{ fontSize: "110px" }}></i></h4>
                        <div className="image">
                        </div>
                    </div>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" name='email' value={user.email}
                        onChange={handleChange} placeholder="Enter email" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" name='password' value={user.password}
                        onChange={handleChange} placeholder="Password" />
                </Form.Group>
                <Form.Group className="mb-3 text-center">
                <Button variant="primary" type="submit" onClick={login}>
                    Login
                </Button>
                </Form.Group>
                
                <Form.Group className="mb-3 text-center">
                    <Form.Label className='mt-3'>OR</Form.Label>
                </Form.Group>
                <Form.Group className="mb-3 text-center">
                <Button variant="primary" type="submit"
                    onClick={register}>
                    Register
                </Button>
                </Form.Group>
            </Form>
        </Container>



            {/* <div className="container">
                <div className="form-box">
                    <div className="header-form">
                        <h4 className="text-primary text-center"><i className="fa fa-user-circle" style={{ fontSize: "110px" }}></i></h4>
                        <div className="image">
                        </div>
                    </div>
                    <div className="body-form">
                        <form>
                            <div className="input-group mb-3">
                                  <input className="form-control" type="email" name='email' value={user.email}
                                    onChange={handleChange} placeholder="Email" />
                            </div>
                            <div className="input-group mb-3">

                                <input type="password" name='password' value={user.password}
                                    onChange={handleChange} className="form-control" placeholder="Password" />
                            </div>
                            <button type="button" className="btn btn-secondary btn-block text-center"
                                onClick={login}>LOGIN</button>
                            <div className="message">
                            </div>
                        </form>
                        <div className="social">
                            <button type="button" className="btn btn-secondary btn-block"
                                onClick={register}>REGISTER</button>
                        </div>
                    </div>
                </div>
            </div> */}
        </>
    )
}

export default Login;